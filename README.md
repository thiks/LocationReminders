# Location Reminders
### Overview
An android app that allows you to create reminders tied to a specific
location or a point of interest.
Created for the course Application development for mobile and ubiquitous
computing at TU Dresden.
